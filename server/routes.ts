const express1 = require('express');
const application = express1.Router();
module.exports = application;
const bodyParser = require('body-parser');
console.log(`Body Parser: ${bodyParser}`);
application.use(express1.urlencoded({extended: true}));
const sanitize = require('mongo-sanitize');
const hashConstructor = require('crypto');
let activeUserID;

let MongoClient = require('mongodb').MongoClient;
let db = null;

MongoClient.connect('mongodb://localhost:27017',
  {useNewUrlParser: true, useUnifiedTopology: true}, (err, client) => {
    db = client.db('test');
    if (err) {
      console.log(err);
    }
  });


application.get('/searchUser', (req: any, res: any) => {
  const name = sanitize(req.query.userName);
  const userPassword = sanitize(req.query.password);
  console.log(`Name = ${name}  Password = ${userPassword}`);
  db.collection('users').findOne({userName: name, password: userPassword}, {userID: 1}, (err, result) => {
    console.log(result);
    if (result) {
      res.send({userID: result.userID, success: 'true'});
    } else {
      res.send('false');
    }
  });
});

application.post('/createUser', (req: any, res: any) => {
  const rawUserName = sanitize(req.body.userName);
  const hashedUserName =  hashConstructor.createHash('sha256').update(rawUserName).digest('hex');
  console.log(`Hashed userName ${hashedUserName}`);
  let add = true;
  db.collection('users').countDocuments({userName: sanitize(req.body.userName)}, (err, count) => {
    console.log(count);
    if (count === 1) {
      add = false;
      console.log('asfasdfasdf');
      res.send({alreadyExists: 'true'});
    } else {
        db.collection('users').insertOne({
          userID: hashedUserName,
          password: sanitize(req.body.password),
          userName: sanitize(req.body.userName)
        });
        db.createCollection(`userNr${hashedUserName}`);
        res.send({alreadyExists: 'false' , hashedID: hashedUserName});
    }
  });
});


class Todo {
  id: number;
  title: string;
  completed: boolean;
  prio ?: number;
  expireDate  ?: string;      // Date string YYYY-MM-DD
}


function addTodo(newTodo: Todo, id) {
  db.collection(String(`userNr${id}`)).insertOne(newTodo);
}

application.get('/getAllTodos', (req: any, res: any) => {
  console.log(`Serching for Todos of user ${req.query.id}`);
  db.collection(`userNr${req.query.id}`).find().toArray((err, result) => {
    console.log(result);
    res.send(result);
  });
});

application.delete('/deleteToDo/:id/:userID', (req: any, res: any) => {
  db.collection(`userNr${req.params.userID}`).deleteOne({id: req.params.id});
  res.send('DELETE ON SERVER CALLED');
});

application.post('/pushTodos', (req: any, res: any) => {
  const newTodo: Todo = req.body.newTodo;
  addTodo(newTodo, req.body.id);
  res.send('Successfully pushed todo: ');
});

application.post('/updateTitle', (req: any, res: any) => {
  const todoTitle = req.body.data;
  const idTodo = req.body.todoID;
  console.log(`Updating Todo with ${idTodo} to ${todoTitle}`);
  db.collection(`userNr${req.body.personalID}`).updateOne({id: idTodo}, {$set: {title: todoTitle}});
});
// Todo userID muss aus call vom client kommen
application.post('/updateInfo', (req: any, res: any) => {
  const todoPrio = req.body.prio;
  const idTodo = req.body.id;
  const todoDate = req.body.date;
  const userID = req.body.userID;
  console.log(`Updating Todo with ${idTodo} to prio: ${todoPrio} and date: ${todoDate}`);
  db.collection(`userNr${userID}`).updateOne({id: idTodo}, {$set: {prio: todoPrio, expireDate: todoDate}});
});

application.post('/updateCompleted', (req: any, res: any) => {
  const todoID = req.body.id;
  const todoCompleted = req.body.compl;
  const userID = req.body.userID;
  console.log(`Updating Todo with ${todoID} completed: ${todoCompleted}`);
  db.collection(`userNr${userID}`).updateOne({id: todoID}, {$set: {completed: todoCompleted}});
});
