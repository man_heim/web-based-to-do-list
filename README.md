# AngularToDoList

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Project Description

The Project is a personal project to learn basics of Angular.
To run the Project it is required to to insatll MongoDB. 
The Angular Front-End can be started by running ng serve and runs on "http://localhost:4200/login" redirection from the default location still needs to be added 
The Node.js server runs with Express.js and needs to be started in the server Project Folder via the yarn serve command. The Server runs on Port 4201 on localhost.
To Connect to the databse where user credentials and Todos are stored a MongoDB Connection on port 27017 is necessary and a collection named "users" needs to be imported to MongoDB.
Collections containing the Todos of a user are automatically created by creating a new user.
Scripts for MongoDB Connections and start scripts still need to be added to the project.

