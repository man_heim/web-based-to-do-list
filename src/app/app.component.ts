import { Component } from '@angular/core';
import * as $ from 'jquery';
import {ToDoListComponent} from './components/to-do-list/to-do-list.component';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  static personalID;
  title = 'angular-toDoList';

}
