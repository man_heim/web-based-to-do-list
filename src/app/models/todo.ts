export class Todo {
  id: number;
  title: string;
  completed: boolean;
  prio ?: number;
  expireDate  ?: string;      // Date string YYYY-MM-DD
}
