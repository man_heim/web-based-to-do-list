import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginPageComponent} from './login-page/login-page.component';
import {ToDoListComponent} from './components/to-do-list/to-do-list.component';


const routes: Routes = [
  { path: 'login', component: LoginPageComponent},
  {path: 'personalToDoList' , component: ToDoListComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LoginPageComponent, ToDoListComponent];
