import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import * as $ from 'jquery';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  userLogin() {
    const userNameInput = String($('#loginUsername').val());
    const passwordInput = String($('#loginPassword').val());
    // if (userNameInput.replace(/\s/g, '') !== '' && passwordInput.replace(/\s/g, '') !== ''  ) {
    $.get('http://localhost:4201/searchUser', {
      userName: userNameInput,
      password: passwordInput
    }, (data) => {
      if (data.success === 'true') {
        console.log(`Login personalID ${data.userID}`);
        this.router.navigate(['./personalToDoList'], {state: {userID: data.userID}});
      } else {
        this.showRegister(true);
      }
    });
  }

  showRegister(failedLogin: boolean) {
    const element = $('#message');
    if (failedLogin) {
      element.html('Wrong Username or Password');
      element.show();
    } else {
      element.hide();
    }
    $('#loginPasswordCheck').show();
    $('#registerButton').show();
  }


  Register() {
    const passwordElement = $('#loginPassword');
    if (passwordElement.val() === $('#loginPasswordCheck').val()) {
      $.post('http://localhost:4201/createUser', {
        password: passwordElement.val(),
        userName: $('#loginUsername').val()
      }, (data) => {
        console.log(data);
        if (data.alreadyExists === 'true') {
          console.log('User already exists');
          const element = $('#message');
          element.show();
          element.html('This Username already exists');
        } else {
          this.router.navigate(['./personalToDoList'], {state: {userID: data.hashedID}});
        }
      });
    } else {
      const element = $('#message');
      element.show();
      element.html('Password and Confirmation do not match');
    }
  }
}
