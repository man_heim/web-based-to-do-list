import {Component, OnInit} from '@angular/core';
import {Todo} from '../../models/todo';
import {Location} from '@angular/common';
import * as $ from 'jquery';
import {AppComponent} from '../../app.component';


@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})


export class ToDoListComponent implements OnInit {

  constructor(private location: Location) {
  }
  static personalID: string;

  allToDos: Todo[] = [];
  toDoDone: Todo[] = [];
  idCounter = 0;
  toDoPending: Todo[] = [];

  ngOnInit(): void {
    const state = this.location.getState();
    // @ts-ignore
    ToDoListComponent.personalID = state.userID;
    console.log(ToDoListComponent.personalID);
    $.get('http://localhost:4201/getAllTodos', {id: ToDoListComponent.personalID}, (data) => {
      this.idCounter = (data as any[])[(data as any[]).length - 1].id;
      let bool = false;
      data.forEach(todo => {
        if (todo.completed === 'true') {
          bool = true;
        } else if (todo.completed === 'false') {
          bool = false;
        }
        console.log(`In ToDoList push to all todos Completed: ${todo.completed}`);
        console.log(`On create todo expireDate: ${todo.date}`);
        this.allToDos.push({
          id: todo.id,
          title: todo.title,
          completed: bool,
          prio: todo.prio,
          expireDate: todo.expireDate
        });
      });
      this.allToDos.forEach(todo => console.log(todo));
      this.allToDos.forEach(todo => {
        console.log('Todo Completed Value: ' + todo.completed);
        if (todo.completed === true) {
          console.log(`Pushing to done`);
          this.toDoDone.push(todo);
        } else {
          console.log('pushig to pending');
          this.toDoPending.push(todo);
        }
      });
    });
  }

  createTodo(todoTitle: string, priority ?: number, date ?: string) {
    if (this.idCounter === undefined) {
      this.idCounter = 0;
    }
    console.log(priority);
    if (priority === undefined) {
      priority = 2;
    }
    console.log(todoTitle);
    console.log('CreateTodocalled');
    this.idCounter++;
    const todo = {id: this.idCounter, title: todoTitle, prio: priority, completed: false, expireDate: date};
    console.log(todo);
    this.allToDos.push(todo);
    this.toDoPending.push(todo);
    $.post('http://localhost:4201/pushTodos',
      {
        newTodo: todo,
        id: ToDoListComponent.personalID
      }
      , (res: any) => {
        console.log(res);
      });
  }

  deleteToDoItem(id: number, completed: boolean) {
    console.log(completed);
    console.log(id);
    this.allToDos = this.allToDos.filter(todo => todo.id !== id);
    if (completed === true) {
      this.toDoDone = this.toDoDone.filter(todo => todo.id !== id);
    } else {
      this.toDoPending = this.toDoPending.filter(todo => todo.id !== id);
    }
  }

  displayPending() {
    const pendingBlock = $('#todoPending') as JQuery;
    if (pendingBlock.is(':visible')) {
      $('#displayPending').html(`(${this.toDoPending.length}) &#9662;`);
      pendingBlock.fadeOut('100');
    } else {
      pendingBlock.fadeIn('100');
      $('#displayPending').html(`(${this.toDoPending.length}) &#9652;`);
    }
  }

  displayDone() {
    const pendingBlock = $('#todoDone') as JQuery;
    if (pendingBlock.is(':visible')) {
      $('#displayDone').html(`(${this.toDoDone.length}) &#9662;`);
      pendingBlock.fadeOut('100');
    } else {
      pendingBlock.fadeIn('100');
      $('#displayDone').html(`(${this.toDoDone.length}) &#9652;`);
    }
  }


  clearTodo() {
    this.toDoDone.forEach(todo => {
      this.deleteElementOnServer(todo.id);
    });
    this.toDoDone = [];
  }

  deleteElementOnServer(id: number) {
    $.ajax({
      url: `http://localhost:4201/deleteToDo/${id}/${ToDoListComponent.personalID}`,
      type: 'DELETE',
      success: (result) => {
        console.log(result);
      },
      error: (callBack) => {
        console.log(callBack);
      }
    });
  }
}
