import {Component, OnInit, Input} from '@angular/core';
import {ToDoListComponent} from '../to-do-list/to-do-list.component';

@Component({
  selector: 'app-detail-creation',
  templateUrl: './detail-creation.component.html',
  styleUrls: ['./detail-creation.component.css']
})
export class DetailCreationComponent implements OnInit {

  @Input() context: ToDoListComponent;

  prio ?: number;
  date ?: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  setPrio() {
    const element = $(`#prio`);
    if (element.attr('src') === './assets/2xclam.PNG') {
      element.attr('src', './assets/3xclam.PNG');
      this.prio = 3;
    } else if (element.attr('src') === './assets/3xclam.PNG') {
      element.attr('src', './assets/1xclam.PNG');
      this.prio = 1;
    } else {
      element.attr('src', './assets/2xclam.PNG');
      this.prio = 2;
    }
  }
  setDate() {
    const element = $(`#dateTillDone`);
    this.date = String(element.val());
    console.log(`This is the date: ${this.date}`);
  }

  reset() {
    $(`#dateTillDone`).val('');
    $(`#prio`).attr('src', './assets/2xclam.PNG');
    this.prio = 2;
  }
}
