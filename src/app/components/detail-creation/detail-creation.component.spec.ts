import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailCreationComponent } from './detail-creation.component';

describe('DetailCreationComponent', () => {
  let component: DetailCreationComponent;
  let fixture: ComponentFixture<DetailCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
