import {Component, OnInit, Input} from '@angular/core';
import {ToDoListComponent} from '../to-do-list/to-do-list.component';
import * as $ from 'jquery';
import {Todo} from '../../models/todo';


@Component({
  selector: 'app-detail-info',
  templateUrl: './detail-info.component.html',
  styleUrls: ['./detail-info.component.css']
})
export class DetailInfoComponent implements OnInit {

  @Input() context: ToDoListComponent;
  @Input() todo: Todo;
  url: string;

  constructor() {
  }

  ngOnInit(): void {
    console.log(`date till done on creation ${this.todo.expireDate}`);
    const prio = (this.todo.prio as number);
    console.log(this.todo.title + '   :   ' + prio + '         :        ' + this.todo.id);
    // tslint:disable-next-line:triple-equals
    if (prio == 1) {
      this.url = './assets/1xclam.PNG';
      console.log('TODO has prio 1    ' + this.todo.id + ' ' + this.todo.title);
      // tslint:disable-next-line:triple-equals
    } else if (prio == 2 || prio == undefined) {
      this.url = './assets/2xclam.PNG';
      console.log('TODO has prio 2        ' + this.todo.id + ' ' + this.todo.title);
    } else {
      this.url = './assets/3xclam.PNG';
      console.log('TODO has prio 3        ' + this.todo.id + ' ' + this.todo.title);
    }
    $(`dateTillDone${this.todo.id}`).attr('value', this.todo.expireDate);
  }

  changePrio() {
    const element = $(`#prio${this.todo.id}`);
    if (this.todo.prio === 1) {
      this.todo.prio = 2;
      element.attr('src', './assets/2xclam.PNG');
    } else if (this.todo.prio === 2) {
      this.todo.prio = 3;
      element.attr('src', './assets/3xclam.PNG');
    } else {
      this.todo.prio = 1;
      element.attr('src', './assets/1xclam.PNG');
    }
  }

  changeDate() {
    this.todo.expireDate = String($(`#dateTillDone${this.todo.id}`).val());
  }
}
