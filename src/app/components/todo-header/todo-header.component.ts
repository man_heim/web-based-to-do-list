import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {AppComponent} from '../../app.component';
import {Router} from '@angular/router';



@Component({
  selector: 'app-todo-header',
  templateUrl: './todo-header.component.html',
  styleUrls: ['./todo-header.component.css']
})
export class TodoHeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  logoutUser() {
    AppComponent.personalID = null;
    this.router.navigate(['./login']).then(r => console.log(r));
  }
}
