import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonClearAllComponent } from './button-clear-all.component';

describe('ButtonClearAllComponent', () => {
  let component: ButtonClearAllComponent;
  let fixture: ComponentFixture<ButtonClearAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonClearAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonClearAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
