import { Component, OnInit, Input } from '@angular/core';
import {ToDoListComponent} from '../to-do-list/to-do-list.component';

@Component({
  selector: 'app-button-clear-all',
  templateUrl: './button-clear-all.component.html',
  styleUrls: ['./button-clear-all.component.css']
})
export class ButtonClearAllComponent implements OnInit {

  @Input() toDoListComponent: ToDoListComponent;
  constructor() { }

  ngOnInit(): void {
  }

  deleteAllDone() {
    this.toDoListComponent.clearTodo();
  }
}
