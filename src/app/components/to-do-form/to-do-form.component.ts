import {Component, OnInit, Input} from '@angular/core';
import {ToDoListComponent} from '../to-do-list/to-do-list.component';
import * as $ from 'jquery';
import {DetailCreationComponent} from '../detail-creation/detail-creation.component';


@Component({
  selector: 'app-to-do-form',
  templateUrl: './to-do-form.component.html',
  styleUrls: ['./to-do-form.component.css'],
})
export class ToDoFormComponent implements OnInit {

  @Input() toDoListComponent: ToDoListComponent;


  constructor() {
  }

  ngOnInit(): void {
  }


  createNewTodo(creator: DetailCreationComponent) {
    const element = $('#createInput');
    this.toDoListComponent.createTodo(String(element.val()), creator.prio, creator.date);
    element.val('');
    creator.reset();
  }

  showMoreInfo() {
    const element = $('app-detail-creation');
    if ((element.is(':hidden'))) {
      element.show();
      $(`#openInfo span`).html('&#9652;');
    } else {
      element.hide();
      $(`#openInfo span`).html('&#9662;');

    }
  }
}
