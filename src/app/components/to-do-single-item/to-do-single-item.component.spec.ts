import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToDoSingleItemComponent } from './to-do-single-item.component';

describe('ToDoSingleItemComponent', () => {
  let component: ToDoSingleItemComponent;
  let fixture: ComponentFixture<ToDoSingleItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToDoSingleItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToDoSingleItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
