import {Component, Input, OnInit} from '@angular/core';
import {Todo} from '../../models/todo';
import {ToDoListComponent} from '../to-do-list/to-do-list.component';
import * as $ from 'jquery';


@Component({
  selector: 'app-to-do-single-item',
  templateUrl: './to-do-single-item.component.html',
  styleUrls: ['./to-do-single-item.component.css']
})
export class ToDoSingleItemComponent implements OnInit {

  @Input() toDoListComponent: ToDoListComponent;
  @Input() todo: Todo;
  unmarked = false;
  marked = false;

  constructor() {
  }

  ngOnInit(): void {
    if (this.todo.completed === true) {
      this.unmarked = false;
      this.marked = true;
    } else {
      this.unmarked = true;
      this.marked = false;
    }
  }

  deleteToDoItem() {
    console.log('deleteCalled');
    this.toDoListComponent.deleteElementOnServer(this.todo.id);
    this.toDoListComponent.deleteToDoItem(this.todo.id, this.todo.completed);
  }

  changeCompleted() {
    if (this.todo.completed === true) {
      this.todo.completed = false;
      this.toDoListComponent.toDoPending.push(this.todo);
      this.toDoListComponent.toDoDone = this.toDoListComponent.toDoDone.filter(todo => todo.id !== this.todo.id);
      this.changeCompletedOnServer(false);
    } else {
      this.todo.completed = true;
      this.toDoListComponent.toDoDone.push(this.todo);
      this.toDoListComponent.toDoPending = this.toDoListComponent.toDoPending.filter(todo => todo.id !== this.todo.id);
      this.changeCompletedOnServer(true);
    }
  }

  changeCompletedOnServer(completed: boolean) {
    $.post('http://localhost:4201/updateCompleted',
      {
        id: this.todo.id,
        compl: completed,
        userID: ToDoListComponent.personalID
      },
      (data) => console.log(data));
  }

  showMoreInfo() {
    const element = $(`#detailInfo${this.todo.id}`);
    if ((element.is(':hidden'))) {
      element.show();
      $(`#openInfo${this.todo.id} span`).html('&#9652;');
    } else {
      element.hide();
      $(`#openInfo${this.todo.id} span`).html('&#9662;');
      $.post('http://localhost:4201/updateInfo',
        {
          prio: this.todo.prio,
          date: this.todo.expireDate,
          id: this.todo.id,
          userID: ToDoListComponent.personalID
        },
        (data) => {
          console.log(data);
        });
    }
  }

  saveTitle() {
    const title = $(`#input${this.todo.id}`).val();
    $.post('http://localhost:4201/updateTitle', {
        data: title,
        personalID: ToDoListComponent.personalID,
        todoID: this.todo.id
      }, (res: any) => {
        console.log(res);
      }
    );
  }
}
