import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule , routingComponents} from './app-routing.module';
import { AppComponent } from './app.component';
import { TodoHeaderComponent } from './components/todo-header/todo-header.component';
import { ToDoSingleItemComponent } from './components/to-do-single-item/to-do-single-item.component';
import { ToDoFormComponent } from './components/to-do-form/to-do-form.component';
import { ButtonClearAllComponent } from './components/button-clear-all/button-clear-all.component';
import { DetailInfoComponent } from './components/detail-info/detail-info.component';
import { DetailCreationComponent } from './components/detail-creation/detail-creation.component';
import {HttpClientModule} from '@angular/common/http';
import * as $ from 'jquery';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    TodoHeaderComponent,
    ToDoSingleItemComponent,
    ToDoFormComponent,
    ButtonClearAllComponent,
    DetailInfoComponent,
    DetailCreationComponent,
    routingComponents,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
